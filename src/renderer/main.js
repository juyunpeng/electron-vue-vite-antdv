import { createApp } from 'vue'
// 全局样式
import '@renderer/common/styles/frame.styl'
import Antd from 'ant-design-vue'
import App from './App.vue'
import 'ant-design-vue/dist/reset.css'
import router from './router'

createApp(App).use(Antd).use(router).mount('#app')
